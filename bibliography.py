from pkg_resources import resource_filename


std_path = resource_filename(__name__, '../resources/bibliography/bibliography.bib')

__all__ = ['citation']


def citation(bibtex):
    r"""
    Add citation to an object

    Args:
        bibtex (str): Containing the bibtex citation for reference

    Returns:
        A citation decorated object
    """

    def citation_decorator(obj):
        obj.bibtex = bibtex
        return obj

    return citation_decorator
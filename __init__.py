r"""
.. module:: MTIpython.reference
    :platform: Unix, Windows
    :synopsis: packages needed for source citation

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from MTIpython.reference import bibliography

r"""
:class:`Bibliography`: object handling all citations and bibliography generations for *MTIpython*
"""

__all__ = ['bibliography']

